/**
 * TopicController
 *
 * @description :: Server-side logic for managing topics
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  posts: function(req, res) {
    Topic.find({}).exec(function(err, data) {
      res.json(data);
    });
  },
  upvote: function(req, res) {
    if (req.param('id') == req.param('authorId')) {
      res.badRequest();
    }
    Topic.find({ authorId: req.param('authorId') }).exec(function(err, topic) {
      if (topic[0].upVotes.includes(req.param('id'))) {
        _.remove(topic[0].upVotes, function(n) {
          return n == req.param('id');
        });
      } else {
        if (topic[0].downVotes.includes(req.param('id'))) {
          _.remove(topic[0].downVotes, function(n) {
            return n == req.param('id');
          });
        } else {
          topic[0].upVotes.push(req.param('id'));
        }
      }

      Topic.update(
        { authorId: req.param('authorId') },
        { upVotes: topic[0].upVotes }
      ).exec(function(err, data) {
        res.json({
          count: data[0].upVotes
        });
      });
    });
  },
  downvote: function(req, res) {
    if (req.param('id') == req.param('authorId')) {
      res.badRequest();
    }
    Topic.find({ authorId: req.param('authorId') }).exec(function(err, topic) {
      if (topic[0].downVotes.includes(req.param('id'))) {
        _.remove(topic[0].downVotes, function(n) {
          return n == req.param('id');
        });
      } else {
        if (topic[0].upVotes.includes(req.param('id'))) {
          _.remove(topic[0].upVotes, function(n) {
            return n == req.param('id');
          });
        } else {
          topic[0].downVotes.push(req.param('id'));
        }
      }

      Topic.update(
        { authorId: req.param('authorId') },
        { downVotes: topic[0].downVotes }
      ).exec(function(err, data) {
        res.json({
          count: data[0].downVotes
        });
      });
    });
  },
  newpost: function(req, res) {
    Topic.create({
      authorId: req.param('id'),
      title: req.param('title'),
      subTitle: req.param('subTitle'),
      postText: req.param('postText'),
      upVotes: [],
      downVotes: []
    }).exec(function(err, post) {
      if (err) {
        return res.serverError(err);
      }
      res.ok();
    });
  },
  findPost: function(req, res) {
    Topic.findOne({
      authorId: req.param('id')
    }).exec(function(err, post) {
      res.json(post);
    });
  },
  newcomment: function(req, res) {
    Comment.create({
      author: req.param('author'),
      owner: req.param('owner'),
      content: req.param('comment')
    }).exec(function(err, comment) {
      if (err) {
        return res.serverError(err);
      }
      res.ok();
    });
  }
};
