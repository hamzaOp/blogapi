/**
 * Topic.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  connection: 'Topics',
  attributes: {
    title: {
      type: 'string'
    },
    authorId: {
      type: 'integer'
    },
    subTitle: {
      type: 'string'
    },
    postText: {
      type: 'string'
    },
    upVotes: {
      type: 'array'
    },
    downVotes: {
      type: 'array'
    },
    comments: {
      collection: 'comment',
      via: 'owner'
    }
  }
};
